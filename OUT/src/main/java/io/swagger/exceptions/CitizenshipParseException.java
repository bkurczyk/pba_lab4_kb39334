package io.swagger.exceptions;

public class CitizenshipParseException extends RuntimeException{
    public CitizenshipParseException() {
        super();
    }

    public CitizenshipParseException(String message) {
        super(message);
    }

    public CitizenshipParseException(String message, Throwable cause) {
        super(message, cause);
    }

    public CitizenshipParseException(Throwable cause) {
        super(cause);
    }

    protected CitizenshipParseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
