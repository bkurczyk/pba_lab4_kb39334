package io.swagger.exceptions;

public class ClientDoesntExistsException extends RuntimeException {
    public ClientDoesntExistsException() {
        super();
    }

    public ClientDoesntExistsException(String message) {
        super(message);
    }

    public ClientDoesntExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public ClientDoesntExistsException(Throwable cause) {
        super(cause);
    }

    protected ClientDoesntExistsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
