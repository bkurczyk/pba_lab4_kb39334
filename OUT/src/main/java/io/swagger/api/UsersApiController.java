package io.swagger.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.ApiParam;
import io.swagger.exceptions.CitizenshipParseException;
import io.swagger.exceptions.ClientAlreadyExistsException;
import io.swagger.exceptions.ClientDoesntExistsException;
import io.swagger.exceptions.UserNotFound;
import io.swagger.model.*;
import io.swagger.model.db.UserDB;
import io.swagger.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-11-03T14:30:00.307+01:00")

@Controller
public class UsersApiController implements UsersApi {
    private UserRepo userRepo;

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @Autowired
    public UsersApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    @PostConstruct
    public void init(){
        List<UserDB> listOfUsers = new ArrayList<UserDB>();
        UserDB user = UserDB.builder().id(UUID.fromString("1c0f1731-d827-4f84-a19e-31875fb5fe71")).name("John").surname("Smith").age(24).personalId("97070808569")
                .citizenship("PL").email("js@gmail.com").build();
        listOfUsers.add(user);
        user = UserDB.builder().id(UUID.fromString("ac455cac-7245-44fd-8d50-8223fbced82b")).name("Eric").surname("Johnson").age(23).personalId("98080908123")
                .citizenship("DE").email("ej@gmail.com").build();
        listOfUsers.add(user);
        user = UserDB.builder().id(UUID.fromString("d9319637-5d38-4979-be4e-be5dcea6128a")).name("Nick").surname("Brown").age(25).personalId("96010108987")
                .citizenship("UK").email("nb@gmail.com").build();
        listOfUsers.add(user);
        userRepo = new UserRepo(listOfUsers);

    }

    @Override
    public Optional<ObjectMapper> getObjectMapper() {
        return Optional.ofNullable(objectMapper);
    }

    @Override
    public Optional<HttpServletRequest> getRequest() {
        return Optional.ofNullable(request);
    }

    @Override
    public ResponseEntity<UserListResponse> getAllUsers() {
        List<User> Users = userRepo.getListOfUsers().stream()
                .map(p->new User(p.getId(),p.getName(),p.getSurname(),p.getAge(),p.getPersonalId(), User.CitizenshipEnum.valueOf(p.getCitizenship()),p.getEmail()))
                .collect(Collectors.toList());
        return ResponseEntity.ok().body(new UserListResponse().usersList(Users).responseHeader(new ResponseHeader().requestId(UUID.randomUUID()).sendDate(new Date(System.currentTimeMillis()))));
    }

    @Override
    public ResponseEntity<UserResponse> getUserById(@ApiParam(value = "",required=true) @PathVariable("id") UUID id) {
        User user = userRepo.getListOfUsers().stream().filter(u->u.getId().equals(id)).findFirst()
                .map(u->new User(u.getId(),u.getName(),u.getSurname(),u.getAge(),u.getPersonalId(),User.CitizenshipEnum.valueOf(u.getCitizenship()),u.getEmail())).orElse(null);
        if(user==null) throw new UserNotFound("User with this Id not found");
        return ResponseEntity.ok().body(new UserResponse().user(user).responseHeader(new ResponseHeader().requestId(UUID.randomUUID()).sendDate(new Date(System.currentTimeMillis()))));
    }

    @Override
    public ResponseEntity<UserResponse> createUser(@ApiParam(value = "User object that has to be added" ,required=true )  @Valid @RequestBody CreateRequest body) {

        User user = body.getUser();
        ValidateCitizenship(user.getCitizenship());
        userRepo.addNewUser(new UserDB(user.getId(),user.getName(),user.getSurname(), user.getAge(), user.getPersonalId(), user.getCitizenship().toString(),user.getEmail()));

        return ResponseEntity.ok().body(new UserResponse().user(user).responseHeader(new ResponseHeader().requestId(body.getRequestHeader().getRequestId()).sendDate(new Date(System.currentTimeMillis()))));

    }

    @Override
    public ResponseEntity<Void> deleteUser(UUID id) {
        userRepo.deleteUserById(id);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<UserResponse> updateUser(UUID id, @Valid UpdateRequest body) {
        User user = body.getUser();
        ValidateCitizenship(user.getCitizenship());
        userRepo.updateUserById(id,new UserDB(id,user.getName(),user.getSurname(), user.getAge(), user.getPersonalId(), user.getCitizenship().toString(),user.getEmail()));
        return ResponseEntity.ok().body(new UserResponse().user(user).responseHeader(new ResponseHeader().requestId(body.getRequestHeader().getRequestId()).sendDate(new Date(System.currentTimeMillis()))));
    }

    private void ValidateCitizenship(User.CitizenshipEnum citizenship)
    {
        if(citizenship == null)
            throw new CitizenshipParseException("Can't parse Citizenship");
    }
}
